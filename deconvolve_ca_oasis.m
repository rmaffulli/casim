function out = deconvolve_ca_oasis(f, win, t_obs_i, dt, ca_imaging_frate,...
    foo, bar, baz, dsampl_factor, varargin)
f = f(1:round(1/(dt*ca_imaging_frate)):end);
% deconvolved spikes using Oasis
try
    [g, sn] = estimate_parameters(f, 2, [.25, .5], 'mean', 100, 1);
    [~, s_oasis, ~] = deconvolveCa(f, 'ar2',...
        'pars',g,...
        'sn',sn,...
        'method','thresholded',...
        'thresh_factor',1,...
        'maxIter',20);
    s_oasis = s_oasis(1:round(t_obs_i*dt*ca_imaging_frate));
    % downsample
    dsamp_oasis = conv(s_oasis,ones(dsampl_factor,1),'full');
    dsamp_oasis = dsamp_oasis(1:dsampl_factor:end);
    
    % out = sum(dsamp_oasis > 0);
    out = mean(dsamp_oasis);
catch
    out = NaN;
end
end