clear all
close all
clc

cols = brewermap(8, 'Dark2');

indicators = {"GCaMP6f" "GCaMP6s"};

% Indicators params from Chen et al.
% GCaMP6f
K_d(1) = 375e-9;
R_f(1) = 51.8;
n_h(1) = 2.27;
max_dF_F(1) = 13.1;     % 160 AP
% GCaMP6s
K_d(2) = 144e-9;
R_f(2) = 63.2;
n_h(2) = 2.9;
max_dF_F(2) = 16.8;     % 160 AP

ca_rest = 50e-9;

hill_eq = @(c,R_f,K_d,n_h) R_f*(1./(1 + (K_d./c).^n_h));
fluo_sat_eq = @(c,ca_rest,K_d,max_dF_F) (c-ca_rest)./(c+K_d)*max_dF_F;

% calcium concentrations array
ca = logspace(-8, -3, 20000);

figure()
hold on
for i=1:length(indicators)
    plot(ca,fluo_sat_eq(ca,ca_rest,K_d(i),max_dF_F(i)),...
        'LineWidth',2,'Color',cols(i,:),'DisplayName',indicators{i});
    set(gca, 'XScale', 'log')
end
legend()