function out = deconvolved_free_ca(f, foo, t_obs_i, bar,...
    ca_imaging_frate, t, ind, baz, buz)

[~, K_d] = get_ca_sat_params(ind);
if strcmp(ind,'GCaMP6s')
    load('CaSim_1EqBindingKinetics_GCaMP6s_constants.mat')
elseif strcmp(ind,'GCaMP6f')
    load('CaSim_1EqBindingKinetics_GCaMP6f_constants.mat')
end
k_off   = casim_constants.k_off;
n_prime = casim_constants.n_prime;
phi     = casim_constants.phi;
alpha   = phi - 1;
B_t     = 10E-6;
ca_rest = 50e-9;
ca_b_rest  = B_t*ca_rest^n_prime/(K_d^n_prime + ca_rest^n_prime);
max_dff = alpha*(B_t*(1e-5^n_prime)/(K_d^n_prime + 1e-5^n_prime) - ca_b_rest)/(B_t + alpha*ca_b_rest);

dt = t(2) - t(1);
ext_rate = 1200;
r = 5e-6;                                           % soma radius (approximated as sphere)
K_M = 0.8e-6;                                       % Sala et al.
vol = 4/3*pi*r^3;                                   % soma volume
area = 4*pi*r^2;                                    % membrane area
v_max = (ca_rest + K_M)^2*ext_rate*vol/area/K_M;    % Calcium extrusion velocity to match ext_rate (taking extrusion flux j_out as in Helmchen et al. "Handbook of neural..." Eqn. 10.13 and 10.14, calculating derivative when ca = ca_rest and setting that equal to ext_rate)
f = f(1:round(1/(dt*ca_imaging_frate)):t_obs_i);
t = t(1:round(1/(dt*ca_imaging_frate)):t_obs_i);

options = optimset('Display','off','FunValCheck','on');

try
    f_spline_fit = fit(t,f,'smoothingspline','smoothingParam',0.999);
catch
    out = NaN;
    return
end
f_spline = feval(f_spline_fit,t);

df_0 = f_spline(1);
if df_0 < 0
    df_0 = 0;
elseif df_0 > max_dff
    df_0 = 0.99*max_dff;
end

ca_deltaF_relation = @(ca) B_t*(ca.^n_prime)./(K_d.^n_prime + ca.^n_prime) - (1/alpha*df_0*(B_t+alpha*ca_b_rest) + ca_b_rest);
try
    ca_0 = fzero(ca_deltaF_relation,[ca_rest 150*ca_rest],options);
catch ME
    if (strcmp(ME.identifier,'MATLAB:fzero:ValuesAtEndPtsSameSign'))
        ca_0 = 1E-5;    % the error above happens for high values of fluorescence: set ca_0 in this case to max available free ca in the cell according to Helmchen
    else
        disp("error")
    end
end

dt = t(2)-t(1);
delta_f_f_prime = [diff(f_spline)/dt; 0];
ca = nan(size(f));
j_out = nan(size(f));
ca(1) = ca_0;
j_out(1) = v_max*area/vol*((ca(1))/(ca(1)+K_M)-(ca_rest)./(ca_rest+K_M));

for i = 1:length(f)-1
    temp = (B_t*alpha*n_prime*(K_d^n_prime)*(ca(i)^(n_prime-1)))/((B_t+alpha*ca_b_rest)*(ca(i)^n_prime+K_d^n_prime)^2);
    ca(i+1) = ca(i) + dt*delta_f_f_prime(i)/temp;
    if ca(i+1) < 0
        ca(i+1) = ca(i);
    end
    k_s = 110;
    k_b = B_t*n_prime*ca(i)^(n_prime-1)*K_d^n_prime/(ca(i)^n_prime + K_d^n_prime)^2;
    
    if ca(i+1) - ca(i) > 7.6E-6/(1 + k_s + k_b)
        ca(i+1) = ca(i) + 7.6E-6/(1 + k_s + k_b);
    end
    if ca(i+1) > 1e-5
        ca(i+1) = 1e-5;
    end
    if ca(i+1) < ca_rest
        ca(i+1) = ca_rest;
    end
    j_out(i+1) = v_max*area/vol*((ca(i+1))/(ca(i+1)+K_M)-(ca_rest)./(ca_rest+K_M));
end
deconv_ca = diff(ca)/dt + ca(2:end)*k_off;
out = mean(deconv_ca);
end
