function [y] = hill_eq(x,A,K,n)
y = A*x.^n./(K^n+x.^n);
end

