close all
clear all
clc
cols = brewermap(8, 'Set2');

% When using the single compartment model as described in Charles et al.,
% CaSim underpredicts strongly the results of fluorescence when experimental
% data go past a deltaF/F of around 2. This seems to be happening for both
% indicators. One possible explanation is that the simple linear model of
% extrusion used in NAOMi does not consider the possibility that the
% calcium pumps would saturate and not be able to extrude calcium anymore.
% One can include a non-linear extrusion saturation as in Eqn. 10.13 and
% 10.14 in Helmchen Handbook of Neural Activity Measurement but this
% requires to know/estimate: v_max, V, A, K_m. V and A can be estimated
% supposing the soma is spherical. Mari suggested using a uses a 7.5 um
% radius (also used by Charles actually). The values of v_max and K_M are
% really hard to find in literature. I have only found a paper by Sala et
% al. "Calcium Diffusion Modelling in a Spherical Neuron" that gives some
% values. K_M is the value of molar concentration at which extrusion is
% half-maximal. Sala et al use K_m = 0.8e-6 M.
% In this script we plot the calcium influx vs ca concentration in
% the cell according to the linearized model used originally in CaSim
% (using a plausible value of the extrusion rate given that Helmchen in
% Handbook of Neural Activity Measurement gives a range of 100 to 2000 Hz
% for the extrusion rate) and see how this compares with the nonlinear
% form given by Eqns 10.13 and 10.14 in Helmchen Handbook of Neural Activity
% Measurement with some hand-tuned values of K_M and v_max, starting
% from the values of Sala et al.

ext_rate = 300;             % Helmchen L5 (800) Helmchen L2/3 (1800) Charles (300)
ca_rest = 50e-9;            % Helmchen
r = 5e-6;                   
K_M = 0.8e-6;               % Sala
ca = linspace(ca_rest, 100*ca_rest, 1000);
V = 4/3*pi*r^3;
A = 4*pi*r^2;
v_max_sala = 2e-12/(1e-2)^2; %from Sala et al.
v_max_ext_rate_helmchen = (ca_rest + K_M)^2*ext_rate*V/A/K_M;


j_out_lin = ext_rate*(ca-ca_rest);
j_out_nonlin_sala = v_max_sala*A/V*((ca)./(ca+K_M)-(ca_rest)./(ca_rest+K_M));
j_out_nonlin_helmchen = v_max_ext_rate_helmchen*A/V*((ca)./(ca+K_M)-(ca_rest)./(ca_rest+K_M));

figure()
hold on
plot(ca,j_out_lin,'DisplayName','Linear approximation','LineWidth',3,'Color',cols(4,:));
plot(ca,j_out_nonlin_sala,'DisplayName','Non-linear form [Sala et al.]','LineWidth',3,'Color',cols(5,:));
plot(ca,j_out_nonlin_helmchen,'DisplayName','Non-linear form with same ext rate as [Helmchen et al.]','LineWidth',3,'Color',cols(6,:));

legend('box','off','location','best')
xlabel('[Ca] (M)')
ylabel('j_{out} (M/s)')
ax = findobj(gcf, 'type', 'axes');
set(ax,'LineWidth',3)
set(ax,'FontSize',20);
set(gca,'TickDir','out');
