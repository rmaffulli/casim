clear all
close all
clc
rng("default")

cols = brewermap(12, 'Set1');

indicators = ["GCaMP6f" "GCaMP6s"];
ind_con = 10E-6;
baseline_ca_rest = 50.0E-9;                              % 50 nMol resting Ca concentration used in CaSim
baseline_rate = 0.5;
rate_limits = [0 15] + baseline_rate;
SFs = [0 1];
sigma_dt_ratio_limits = [0.03 10];
ca_smoothing_win_duration = 0.3;               % s

signoiseratio = 15;
t_max = 1;
dt = 1E-3;                                      % e-phys timestep
ca_imaging_frate = 100;                         % frame rate to be used by calcium imaging data. They are effectively generated at the e-phys timestep dt and then downsampled to simulate various acquisition rates
dt_plots = 1e-3;                                % timestep for plots
n_trials = 100;                                  % number of realizations at each average rate, sigma, indicator and initial calcium concentration
t = linspace(0,t_max,t_max/dt)';

n_traces = 300;

figure(1)
set(gca,'visible','off')
pan = panel();
pan.de.margin = 20;
pan.pack(length(SFs),length(indicators));

for i = 1:length(indicators)
    ind = indicators(i);
    [~, K_d] = get_ca_sat_params(ind);
    for s = 1:length(SFs)
        sf = SFs(s);
        for tr=1:n_traces
            sigma = sigma_dt_ratio_limits(1) + (sigma_dt_ratio_limits(2)-sigma_dt_ratio_limits(1))*rand;
            r = rate_limits(1) + (rate_limits(2)-rate_limits(1))*rand;
            [rate, ~] = gen_rate(t,sigma,r,baseline_rate,0);
            sp = poisson_spike_gen(t,rate,0);
            %sf = sat_factor_limits(1) + (sat_factor_limits(2)-sat_factor_limits(1))*rand;
            ca_0 = sf*K_d + baseline_ca_rest;
            [t_fl, fl(i,tr,:), ca_gt(i,tr,:)] = generate_trace_CaSim(t,sp,ind,...
                ca_0,signoiseratio,'PlotTraces', false);
            
            ca_inf(i,tr,:) = estimate_free_ca_based_on_f(t,squeeze(fl(i,tr,:)),...
                ind_con,ca_0,ind,ca_smoothing_win_duration,squeeze(ca_gt(i,tr,:)));
            
        end
        
        fl_2d = reshape(fl(i,:),[],1);
        ca_gt_2d = reshape(ca_gt(i,:),[],1);
        ca_inf_2d = reshape(ca_inf(i,:),[],1);
        
        % fit hill eq to experimental inferred calcium data
        ft = fittype('hill_eq(x,A,k,n)');
        x = ca_inf_2d;
        y = fl_2d;
        fitresult{i,s} = fit(x, y, ft, 'StartPoint', [3.5,1e-6,2],...
            'Lower', [2 1e-7 1], 'Upper', [4 1e-5 4]);
        
        pan(s,i).select();
        title(ind,'FontSize',20);
        scatter(ca_inf_2d,fl_2d,2,...
            'MarkerEdgeColor',cols(9,:),...
            'MarkerFaceColor',cols(9,:),...
            'DisplayName',"SF = " + num2str(sf))
        hold on
        h = plot(fitresult{i,s});
        set(h,'DisplayName',"Data fit, SF = " + num2str(sf));
        set(h,'LineWidth',2);
        set(h,'Color','k');
        x_ci = linspace(min(ca_inf_2d), max(ca_inf_2d), 1000);
        ci_est = predint(fitresult{i,s},x_ci,0.95,'obs');
        h = plot(x_ci,ci_est,'--','Color','k',...
            'LineWidth',2);
        set( get( get( h(1), 'Annotation'), 'LegendInformation' ), 'IconDisplayStyle', 'off' );
        set( get( get( h(2), 'Annotation'), 'LegendInformation' ), 'IconDisplayStyle', 'off' );
        
        % fit hill eq to graound truth calcium data
        ft = fittype('hill_eq(x,A,k,n)');
        x = ca_gt_2d;
        y = fl_2d;
        fitresult{i,s} = fit(x, y, ft, 'StartPoint', [3.5,1e-6,2],...
            'Lower', [2 1e-7 1], 'Upper', [4 1e-5 4]);
        h = plot(fitresult{i,s});
        set(h,'DisplayName',"Ground truth data fit, SF = " + num2str(sf));
        set(h,'LineWidth',2);
        set(h,'Color',cols(1,:));
        x_ci = linspace(min(ca_inf_2d), max(ca_inf_2d), 1000);
        ci_est = predint(fitresult{i,s},x_ci,0.95,'obs');
        h = plot(x_ci,ci_est,'--','Color',cols(1,:),...
            'LineWidth',2);
        set( get( get( h(1), 'Annotation'), 'LegendInformation' ), 'IconDisplayStyle', 'off' );
        set( get( get( h(2), 'Annotation'), 'LegendInformation' ), 'IconDisplayStyle', 'off' );
        
        xlabel("[Ca]",'FontSize',20)
        ylabel("\Delta F/ F",'FontSize',20)
        legend('box','off','location','southeast','FontSize',12)
        ylim([-0.5 5])
        xlim([min(ca_inf_2d) max(ca_inf_2d)])
    end
end


%% auxiliary functions

function [rate, time_tot] = gen_rate(t,sigma,mean_rate,baseline_rate,buff_time)
% buff_time is the buffer time (scalar) with zero reate to be appended to t
% needed to correctly find f_0
if buff_time == 0
    time_tot = t;
else
    dt = t(2) - t(1);
    t_tot = t(end) + buff_time;
    time_tot = linspace(0,t_tot,t_tot/dt)';
end
rate = normpdf(t,0.25*max(t),sigma);
rate = rate/mean(rate)*(mean_rate-baseline_rate);
rate = rate + baseline_rate;
% append buffer time with zero rate
if buff_time ~= 0
    rate = [rate; zeros(length(time_tot)-length(t),1)];
end
end

function ca = estimate_free_ca_based_on_f(t,fl,B_t,ca_0,indicator,ca_smoothing_win_duration,ca_gt)
[~, K_d] = get_ca_sat_params(indicator);
if strcmp(indicator,'GCaMP6s')
    load('CaSim_1EqBindingKinetics_GCaMP6s_constants.mat')
elseif strcmp(indicator,'GCaMP6f')
    load('CaSim_1EqBindingKinetics_GCaMP6f_constants.mat')
end
n_prime =   casim_constants.n_prime;
phi     =       casim_constants.phi;
alpha   = phi - 1;
ca_b_0  = B_t*ca_0^n_prime/(K_d^n_prime + ca_0^n_prime);
ext_rate = 300;
ca_rest = 50e-9;
r = 5e-6;                                           % soma radius (approximated as sphere)
K_M = 0.8e-6;                                       % Sala et al.
vol = 4/3*pi*r^3;                                   % soma volume
area = 4*pi*r^2;                                    % membrane area
v_max = (ca_rest + K_M)^2*ext_rate*vol/area/K_M;    % Calcium extrusion velocity to match ext_rate (taking extrusion flux j_out as in Helmchen et al. "Handbook of neural..." Eqn. 10.13 and 10.14, calculating derivative when ca = ca_rest and setting that equal to ext_rate)


ca = nan(size(fl));
j_out = nan(size(fl));
ca(1) = ca_0;
j_out(1) = 0;
fl_old  = fl;
dt = t(2) - t(1);

%fl = smoothdata(fl,'movmean',round(ca_smoothing_win_duration/dt));
y = fit(t,fl,'smoothingspline','smoothingParam',0.999);
fl = feval(y,t);

delta_f_f_prime = [diff(fl)/dt; 0];

for i = 1:length(fl)-1
    temp = (B_t*alpha*n_prime*(K_d^n_prime)*(ca(i)^(n_prime-1)))/((B_t+alpha*ca_b_0)*(ca(i)^n_prime+K_d^n_prime)^2);
    ca(i+1) = ca(i) + dt*delta_f_f_prime(i+1)/temp;
    if ca(i+1) < 0
        ca(i+1) = ca(i);
    end
    k_s = 110;
    k_b = B_t*n_prime*ca(i)^(n_prime-1)*K_d^n_prime/(ca(i)^n_prime + K_d^n_prime)^2;
    
    if ca(i+1) - ca(i) > 7.6E-6/(1 + k_s + k_b)
        ca(i+1) = ca(i) + 7.6E-6/(1 + k_s + k_b);
    end
    
    j_out(i+1) = v_max*area/vol*((ca(i+1))/(ca(i+1)+K_M)-(ca_rest)./(ca_rest+K_M));
end

ca(ca > 1e-5) = 1e-5;

ca(ca < ca_rest) = ca_rest;
%     integ = trapz(t, ca-ca(1));
%     out = integ/t(end);
%     out = mean(ca-ca(1));
%    out = (ca(end) - ca(1))/t(end);
out = ca(end) - ca(1) + 100*(trapz(t, (ca-ca_rest)));
out = ca(end) - ca(1) + (trapz(t, j_out));
k_b = B_t*n_prime*mean(ca)^(n_prime-1)*K_d^n_prime/(mean(ca)^n_prime + K_d^n_prime)^2;
k_b = trapz(t,B_t*n_prime*(ca).^(n_prime-1)*K_d^n_prime./((ca).^n_prime + K_d^n_prime).^2)/t(end);
k_b = mean(B_t*n_prime*(ca).^(n_prime-1)*K_d^n_prime./((ca).^n_prime + K_d^n_prime).^2);
out = out*(1+k_s+k_b)/7.6E-6;
end





