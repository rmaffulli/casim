function out = integral_f(f, win, t_obs_i, dt, ca_imaging_frate,...
    t, varargin)
%f = movmean(f, win);
f = f(1:round(1/(dt*ca_imaging_frate)):t_obs_i);
integ = trapz(t(1:round(1/(dt*ca_imaging_frate)):t_obs_i), f-f(1));
out = integ/t(t_obs_i);
%out = max([integ/t(t_obs_i), 0]);
end