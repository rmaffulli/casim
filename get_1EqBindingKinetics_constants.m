function [k_off, n_prime, phi] = get_1EqBindingKinetics_constants(indicator)
switch indicator
    case {'GCaMP6s','gcamp6s'}
        load('CaSim_1EqBindingKinetics_GCaMP6s_constants.mat')
    case {'GCaMP6f','gcamp6f'}
        load('CaSim_1EqBindingKinetics_GCaMP6f_constants.mat')
end
k_off = casim_constants.k_off;
n_prime = casim_constants.n_prime;
phi = casim_constants.phi;
end

