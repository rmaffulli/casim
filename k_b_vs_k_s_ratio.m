clear all
close all
clc

cols = brewermap(8, 'Dark2');

k_s = 110;                                  % Default for mouse layer 2/3: from Helmchen & Tank 2011
ca_rest = 50e-9;                            % Helmchen
ind_con = 10E-6;                            % Lutcke uses 50e-6 Zariwala 2012, Huber 2012 suggest lower values (GCaMP3), Charles uses 10e-6 in the published version of NAOMi
n = 4;

indicators = {"GCaMP6f", "GCaMP6s"};

ca = linspace(ca_rest,1E2*ca_rest,1E3);
figure()
hold on

for i = 1:length(indicators)
    indicator = indicators{i};
    % Indicators params from Chen et al.
    if strcmp(indicator,'GCaMP6s')
        K_d = 144e-9;
    else
        K_d = 375e-9;
    end
    k_b = ind_con.*n.*ca.^(n-1).*K_d.^n./(ca.^n + K_d^n).^2;
    
    plot(ca,k_b/k_s,'DisplayName',indicator,'LineWidth',2,'Color',cols(i,:))
end

legend()
box off
xlabel('[Ca] [M]');
ylabel('k_b/k_s [AU]');