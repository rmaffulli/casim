close all
clear all
clc

dt = 1e-3;
t = 0:dt:2;
sp_i = 3:500;
sp = zeros(size(t));

sp(sp_i) = 1;

[t_fl, fl, ~, ~] = generate_trace_CaSim(t', sp', "GCaMP6f",...
    0, 0);

plot(t_fl,fl);