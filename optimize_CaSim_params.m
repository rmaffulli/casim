clear all
close all
clc
rng("default")

plot_traces_casim = false;
model = "1EqBindingKinetics";            % "DoubleExpConv" "1EqBindingKinetics"

n_restart = 100;
baseDir   = "/home/rmaffulli/Documents/NITPaper/CalciumImagingSpikeConvolution/ExperimentalData/cai-1/";
%baseDir = "/home/rmaffulli/Documents/NITPaper/CalciumImagingSpikeConvolution/ExperimentalData/WeiEtAl2020/";
gcampType = '6m';	% Currently set up to fit 6s or 6f data
prcntile_l = 10;
prcntile_h = 70;

switch gcampType
    case '6f'
        %datafile = "GCaMP6f_11cells_Chen2013/processed_data/data_20120521_cell2_003_extracted_single_spike_rmaffulli.mat";
        datafile = "GCaMP6f_11cells_Chen2013/processed_data/data_20120521_cell5_007.mat"; %(ok)
        %datafile = "GCaMP6f_11cells_Chen2013/processed_data/data_20120502_cell1_004.mat";
        indicator = 'GCaMP6f';
    case '6s'
        %datafile = "GCaMP6s_9cells_Chen2013/processed_data/data_20120417_cell3_001_extracted_single_spike_rmaffulli.mat"
        %datafile = "GCaMP6s_9cells_Chen2013/processed_data/data_20120515_cell1_004.mat";
        datafile = "GCaMP6s_9cells_Chen2013/processed_data/data_20120515_cell1_006.mat";
        %datafile = "GCaMP6s_9cells_Chen2013/processed_data/data_20120515_cell1_003.mat"; %(ok)
        
        %load([baseDir, 'GCaMP6s_9cells_Chen2013/processed_data/data_20120515_cell1_004.mat'])
        %datafile = "GCaMP6sTG/GP4.3highzoom/141104_cell3_004_para.mat"
        indicator = 'GCaMP6s';
    case '6m'
        indicator = 'GCaMP6m';
    otherwise
        error('Unknown GCaMP type!')
end

% we have no exp traces for 6m so only optimize nprime and phi based on
% data reported in Chen et al. This only works for 1EqBindingKinetics
if ~strcmp(indicator,"GCaMP6m")
    datafile = convertCharsToStrings(datafile);
    if contains(baseDir,'AllenInstitute')
        [t_fl_exp, df_f_exp, sp] = load_allen_institute(baseDir + datafile,...
            prcntile_l, prcntile_h, true);
    elseif contains(baseDir, 'cai-1')
        if contains(datafile, '_extracted_single_spike_rmaffulli.mat')
            load(baseDir + datafile);
            t_fl_exp = t;
            df_f_exp = fl;
        else
            [t_fl_exp, df_f_exp, sp] = load_cai1(baseDir + datafile,...
                prcntile_l, prcntile_h, true);
        end
    elseif contains(baseDir, 'cai-3')
        [t_fl_exp, df_f_exp, sp] = load_cai3(baseDir + datafile,...
            prcntile_l, prcntile_h, true);
    elseif contains(baseDir, 'Wei')
        [t_fl_exp, df_f_exp, sp] = load_wei_et_al(baseDir + datafile,...
            prcntile_l, prcntile_h, true);
    else
        error('unable to process experimental data file')
    end
    
    spike_time = t_fl_exp(sp==1);
    end_time = t_fl_exp(end);
    dt = 1e-3; % this time step is enough not to miss any spike in all cai-1 experimental traces. We are fitting using this value and will be always generating data using this deltat
    t = linspace(0,end_time,floor(end_time/dt));
    sp = zeros(size(t));
    sp_indices_CaSim_rate = spike_time(spike_time < end_time);
    sp_indices_CaSim_rate = round(sp_indices_CaSim_rate/dt);
    sp(sp_indices_CaSim_rate) = 1;
end
errfunDoubleConvKern = @(z) evaluate_rse_DoubleConvKernel(t, sp, t_fl_exp,...
    df_f_exp, indicator, 0, z, model,...
    plot_traces_casim);

errfun_dynamic_range = @(z) evaluate_dyn_range_err(indicator, z);

%tried also the following:
% opts = optimoptions('fmincon','Display','off','OptimalityTolerance',1e-6,...
%     'MaxIterations',100','MaxFunctionEvaluations',100,...
%     'StepTolerance',1E-6);
% the optimization using 1000 restarts gives practically identical final
% results, but using the settings below gives less disperse results,
% hinting that the optimization converges better using the settings below
opts = optimoptions('fmincon','Display','off','OptimalityTolerance',1e-8,...
    'MaxIterations',1000','MaxFunctionEvaluations',1000,...
    'StepTolerance',1E-8);
% optimization bounds
if strcmp(model,"1EqBindingKinetics")
    % k_off: Helmchen says 1 to 1000 in Handbook, GCaMP6 k_off are in the order of 1E0 (Chen et al.)
    % n_prime: Helmchen pg 7 handbook mentions that 0.7 < n < 3.8, Inoue et al
    % says between 2 to 4. Also one must consider that the real number of binding sites (4) is an upper bound to n_prime (see https://doi.org/10.1371/journal.pcbi.1003106)
    % phi: given by me
    l_bnd = [0.5,2,1e-2];
    u_bnd = [100,4,100];
    err = Inf(n_restart,1);
    phierr = Inf(n_restart,1);
elseif strcmp(model,"DoubleExpConv")
    l_bnd = [1E-5,1e-4,1e-4];
    u_bnd = [1,1e-2,1];
    err = Inf(n_restart,1);
end
model_pars = nan(n_restart,3);

% fit only part of the function
parfor kk = 1:n_restart
    kk
    % if using 1EqBindingKinetics optimize separately phi for dynamic
    % range, then optimize k_off and n_prime (dynamic range does not depent
    % on k_off and n_prime as they only affect dynamics and not steady
    % state conditions
    if strcmp(model,"1EqBindingKinetics")
        % optimize phi and n_prime
        [model_pars_tmp1, dynrangerrtmp, dynrangexitflag(kk)] = fmincon(errfun_dynamic_range,...
            l_bnd+rand.*(u_bnd-l_bnd),[],[],[], [],l_bnd,u_bnd,[],opts);
                
        if ~strcmp(indicator,"GCaMP6m")
            % define opt function (for 1EqBindingKinetics must be defined here
            % as it depends on the value of phi_tmp, which is updated
            % continuously)
            errfun1EqBindingKinetics = @(z) evaluate_rse_1EqBindingKinetics(t, sp, t_fl_exp,...
                df_f_exp, indicator, 0, model_pars_tmp1, z, model,...
                plot_traces_casim);
            
            % optimize n_prime and k_off, starting point is phi_tmp
            [model_pars_tmp2, errtmp, exitflag(kk)] = fmincon(errfun1EqBindingKinetics,...
                l_bnd(1)+rand.*(u_bnd(1)-l_bnd(1)),[],[],[], [],l_bnd(1),u_bnd(1),[],opts);
            
        else
            % cannot optimize k_off for GCaMP6m as it requires simultaneous
            % ephys and ophys data which are not available
            model_pars_tmp2 = 2.06; % [Hz], k_off value for GCaMP6m as in Chen et al. Suppl. Mat.
            errtmp = 0;
            exitflag(kk) = 0;
        end
        err(kk)  = errtmp;
        dynrangerr(kk) = dynrangerrtmp;
        model_pars(kk,:) = [model_pars_tmp2 model_pars_tmp1(2) model_pars_tmp1(3)];
        
    elseif strcmp(model,"DoubleExpConv")
        % optimize all three params at the same time
        [model_pars_tmp, errtmp, exitflag(kk)] = fmincon(errfunDoubleConvKern,...
            l_bnd+rand(1,3).*(u_bnd-l_bnd),[],[],[], [],l_bnd,u_bnd,[],opts);
        err(kk)  = errtmp();
        model_pars(kk,:) = model_pars_tmp;
    end
    
    if mod(kk,50) == 0 
        fprintf('*');
    end
end
fprintf('\n')

%% Plot traces
figure()
pan = panel();
pan.de.margin = 20;
pan.pack(1,3);
pan(1,1).select();
violinplot(model_pars(:,1));
if strcmp(model,"1EqBindingKinetics")
    title('k_{off}');
else
    title('A');
end
pan(1,2).select();
violinplot(model_pars(:,2));
if strcmp(model,"1EqBindingKinetics")
    title('n');
else
    title('t_{on}');
end
pan(1,3).select();
violinplot(model_pars(:,3));
if strcmp(model,"1EqBindingKinetics")
    title('\phi');
else
    title('t_{off}');
end
savefig(indicator + "_" + model + "_BoundCaConvolutionConstantsAndError.fig")

figure()
scatter3(model_pars(:,1),model_pars(:,2),model_pars(:,3),5,err)
if strcmp(model,"1EqBindingKinetics")
    xlabel('k_{off}')
    ylabel('n')
    zlabel('\phi');
else
    xlabel('A')
    ylabel('n')
    zlabel('t_{off}')
end
savefig(indicator + "_" + model + "_BoundCaConvolutionConstantsViolinPlots.fig")

[~,min_err_idx] = min(err);
if strcmp(model,"1EqBindingKinetics")
    [~,min_dynerr_idx] = min(dynrangerr);
    casim_constants.k_off    = model_pars(min_err_idx,1);
    casim_constants.n_prime  = model_pars(min_err_idx,2);
    casim_constants.phi      = model_pars(min_dynerr_idx,3);
    %calculate casim on the whole part and plot
    if ~strcmp(indicator,"GCaMP6m")
        [t_fl, fl, ~, ~] = generate_trace_CaSim(t', sp', indicator,...
            0, 0, 'ModelConstants', [casim_constants.k_off, casim_constants.n_prime,...
            casim_constants.phi], 'Model', model);
    end
else
    casim_constants.ca_amp   = model_pars(min_err_idx,1);
    casim_constants.t_on     = model_pars(min_err_idx,2);
    casim_constants.t_off    = model_pars(min_err_idx,3);
    %calculate casim on the whole part and plot
    [t_fl, fl, ~, ~] = generate_trace_CaSim(t', sp', indicator,...
        0, 0, 'ModelConstants', [casim_constants.ca_amp, casim_constants.t_on,...
        casim_constants.t_off], 'Model', model);
end

if ~strcmp(indicator,"GCaMP6m")
    figure();
    plot(t_fl_exp,df_f_exp, 'b', t_fl,fl, '.-r')
    hold on;
    scatter(spike_time,zeros(1,length(spike_time)),20,'k.');
    set(gca,'TickDir','out');
    box off
    xlabel('Time (s)');
    ylabel('\Delta F/F [-]')
    set(gcf,'color',[1,1,1])
    savefig(indicator + "_" + model + "_CasimVsExpTrace.fig")
end

save(indicator + "_" + model + "_CasimOptimizedWorkspace.mat")
save("CaSim_" + model + "_" + indicator + "_constants.mat", 'casim_constants')

%% Auxiliary functions

function err = evaluate_rse_DoubleConvKernel(t, sp, t_fl_exp, df_f_exp, indicator,...
    ca_0, model_constants, model, plot_traces_casim)
if ~iscolumn(t)
    t = t';
end
if ~iscolumn(sp)
    sp = sp';
end
if ~iscolumn(t_fl_exp)
    t_fl_exp = t_fl_exp';
end
if ~iscolumn(df_f_exp)
    df_f_exp = df_f_exp';
end
[t_fl, fl, ~, ~] = generate_trace_CaSim(t, sp, indicator, ca_0, 0,...
    'Model', model, 'ModelConstants', model_constants, ...
    'PlotTraces', plot_traces_casim);

interp_casim_fl = interp1(t_fl,fl,t_fl_exp,'linear','extrap');

p = prctile(df_f_exp, [0 100]);
indices = (df_f_exp>p(1) & df_f_exp<p(2));

err = norm(df_f_exp(indices)-interp_casim_fl(indices))/sqrt(sum((df_f_exp(indices)-mean(df_f_exp(indices))).^2));
end

function err = evaluate_rse_1EqBindingKinetics(t, sp, t_fl_exp, df_f_exp, indicator,...
    ca_0, phi, model_constants, model, plot_traces_casim)
if ~iscolumn(t)
    t = t';
end
if ~iscolumn(sp)
    sp = sp';
end
if ~iscolumn(t_fl_exp)
    t_fl_exp = t_fl_exp';
end
if ~iscolumn(df_f_exp)
    df_f_exp = df_f_exp';
end
[t_fl, fl, ~, ~] = generate_trace_CaSim(t, sp, indicator, ca_0, 0,...
    'Model', model, 'ModelConstants', [model_constants phi(2) phi(3)], ...
    'PlotTraces', plot_traces_casim);

interp_casim_fl = interp1(t_fl,fl,t_fl_exp,'linear','extrap');

p = prctile(df_f_exp, [0 100]);
indices = (df_f_exp>p(1) & df_f_exp<p(2));

err = norm(df_f_exp(indices)-interp_casim_fl(indices))/sqrt(sum((df_f_exp(indices)-mean(df_f_exp(indices))).^2));
end

function err = evaluate_dyn_range_err(indicator, model_params)
% target values are taken from Chen et al., Supp. Tab. 1 (160AP in
% dissociated culture)
if strcmp(indicator,'GCaMP6s')
    target_max_dff = 16.8;
elseif strcmp(indicator,'GCaMP6f')
    target_max_dff = 13.1;
elseif strcmp(indicator,'GCaMP6m')
    target_max_dff = 11.7;
end
max_dff = calculateDynamicRange(indicator, model_params);
err = abs(target_max_dff-max_dff);
end
