% close all
clear all
clc

rng('default')

% color palettes
cols = brewermap(8, 'Dark2');
raster_cols = brewermap(12, 'Blues');
blues = brewermap(8, 'Blues');


%% definition of main data
indicators = ["GCaMP6f"];%, "GCaMP6s"];

baseline_rate = 0.5;                          % baseline rate
sigma = 10;
SNR = 15;                             % signal to noise ratio
t_max = 2;                                    % observation time through which we count spikes and measure metrics
dt = 1E-3;                                      % e-phys timestep
imaging_frame_rate = 30;                % frame rate to be used by calcium imaging data. They are effectively generated at the e-phys timestep dt and then downsampled to simulate various acquisition rates
dt_plots = 1e-3;                                % timestep for plots
n_stats_runs = 100;                             % number of runs
rate = 2;
ind_con = 5e-6;

time_max = linspace(0,t_max,t_max/dt);
rate = gen_rate(time_max,sigma,rate,baseline_rate,0);
spikes = poisson_spike_gen(time_max,rate,0);

spikes = zeros(size(spikes));
spikes(round(0.2/dt)) = 1;
spikes(round(0.5/dt)) = 1;
spikes(round(1.5/dt)) = 1;

for i = 1:length(indicators)

    ind = indicators(i);
    [t_fl, dff] = generate_trace_CaSim(time_max',spikes',ind,ind_con,SNR);
    
    spike_times = time_max(spikes == 1);
    figure()
    hold on
    plot(t_fl,dff)
    plot(spike_times, zeros(size(spike_times)),...
        'LineStyle', 'none',...
        'marker','o',...
        'MarkerSize',3,...
        'MarkerEdgeColor',cols(5,:),...
        'MarkerFaceColor',cols(5,:));
end


%% auxiliary functions
function [rate, time_tot] = gen_rate(t,sigma,mean_rate,baseline_rate,buff_time)
% buff_time is the buffer time (scalar) with zero reate to be appended to t
% needed to correctly find f_0
if buff_time == 0
    time_tot = t;
else
    dt = t(2) - t(1);
    t_tot = t(end) + buff_time;
    time_tot = linspace(0,t_tot,t_tot/dt);
end
rate = normpdf(t,0.25*max(t),sigma);
rate = rate/mean(rate)*(mean_rate-baseline_rate);
rate = rate + baseline_rate;
% append buffer time with zero rate
if buff_time ~= 0
    rate = [rate zeros(1,length(time_tot)-length(t))];
end
end
