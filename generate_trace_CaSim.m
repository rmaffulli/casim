function [t_fl, fl, ca, ca_b] = generate_trace_CaSim(t, sp,...
    indicator, ca_0, SNR, varargin)
%%% generates fluorescence traces given the spike vector sp and indicator

assert(...
    strcmp(indicator,'GCaMP6s') || ...
    strcmp(indicator,'GCaMP6f') || ...
    strcmp(indicator,'GCaMP6m'));
assert(numel(t) == numel(sp))
assert(iscolumn(t))
assert(iscolumn(sp))

% parse inputs for named arguments
validationFcnModel = @(m) assert(isstring(m) ...
    & any(strcmp(m,["DoubleExpConv" "1EqBindingKinetics"])),...
    "'Model' option can only be one of ['DoubleExpConv'" + ...
    " '1EqBindingKinetics'");
validationFcnConstants = @(m) assert(length(m) == 3);
ip = inputParser;
ip.FunctionName = 'generate_trace_CaSim';
addParameter(ip,'ModelConstants',[],validationFcnConstants);
addParameter(ip,'Model',"1EqBindingKinetics",validationFcnModel); 
addParameter(ip,'PlotTraces',false,@islogical)
addParameter(ip,'IndConc',10e-6, @(m) m > 0);
ip.parse(varargin{:});

if strcmp(ip.Results.Model, "DoubleExpConv")
    double_exp_conv_model = true;
    fluo_sat_eq = @(c, ca_rest, K_d, max_dF_F) ...
        (c-ca_rest)./(c+K_d)*max_dF_F;
else
    double_exp_conv_model = false;
end

% Indicators params from Chen et al.
if strcmp(indicator,'GCaMP6s')
    K_d = 144e-9;
    if double_exp_conv_model
        max_dF_F = 16.8;    % 160 AP
    end
elseif strcmp(indicator,'GCaMP6f')
    K_d = 375e-9;
    if double_exp_conv_model
        max_dF_F = 13.1;     % 160 AP
    end
else
    K_d = 167e-9;
    if double_exp_conv_model
        max_dF_F = 11.7;     % 160 AP
    end
end

if ~isempty(ip.Results.ModelConstants)
    if double_exp_conv_model
        A = ip.Results.ModelConstants(1);
        t_on = ip.Results.ModelConstants(2);
        t_off = ip.Results.ModelConstants(3);
    else
        k_off = ip.Results.ModelConstants(1);
        n_prime = ip.Results.ModelConstants(2);
        phi = ip.Results.ModelConstants(3);
        k_on = k_off/K_d^n_prime;                   % see eqn. 10.7 Helmchen's Handbook of neural activity measurement (for cooperative binding he assumes K_a = K_d^n_prime)
    end
else
    if double_exp_conv_model
        % load optimized values
        if strcmp(indicator,'GCaMP6s')
            load('CaSim_DoubleExpConv_GCaMP6s_constants.mat')
        elseif strcmp(indicator,'GCaMP6f')
            load('CaSim_DoubleExpConv_GCaMP6f_constants.mat')
        end
        A =     casim_constants.ca_amp;
        t_on =  casim_constants.t_on;
        t_off = casim_constants.t_off;
    else
        % load optimized values
        if strcmp(indicator,'GCaMP6s')
            load('CaSim_1EqBindingKinetics_GCaMP6s_constants.mat')
        elseif strcmp(indicator,'GCaMP6f')
            load('CaSim_1EqBindingKinetics_GCaMP6f_constants.mat')
        else
            load('CaSim_1EqBindingKinetics_GCaMP6m_constants.mat')
        end
        k_off =     casim_constants.k_off;
        n_prime =   casim_constants.n_prime;
        phi =       casim_constants.phi;
        k_on =      k_off/K_d^n_prime;              % see eqn. 10.7 Helmchen's Handbook of neural activity measurement (for cooperative binding he assumes K_a = K_d^n_prime)
    end
end

% define extrusion and influx properties
non_lin_extrusion = true;
delta_ca_sp = 7.6E-6;                               % Helmchen, Lutcke
ext_rate = 1200;                                     % Helmchen L5 (800) Helmchen L2/3 (1800) Charles (300)
k_s = 110;                                          % Default for mouse layer 2/3: from Helmchen & Tank 2011
ind_con = ip.Results.IndConc;                       % Lutcke uses 50e-6 Zariwala 2012, Huber 2012 suggest lower values (GCaMP3), Charles uses 10e-6 in the published version of NAOMi
ca_rest = 50e-9;                                    % Helmchen
r = 5e-6;                                           % soma radius (approximated as sphere)
K_M = 0.8e-6;                                       % Sala et al.
vol = 4/3*pi*r^3;                                   % soma volume
area = 4*pi*r^2;                                    % membrane area
v_max = (ca_rest + K_M)^2*ext_rate*vol/area/K_M;    % Calcium extrusion velocity to match ext_rate (taking extrusion flux j_out as in Helmchen et al. "Handbook of neural..." Eqn. 10.13 and 10.14, calculating derivative when ca = ca_rest and setting that equal to ext_rate)

% cooperative binding
% Paper: Inoue et al., 2019, Cell 177, 1346–1360
% May 16, 2019 ª 2019 Elsevier Inc.
% https://doi.org/10.1016/j.cell.2019.04.007
% "Consistently, although signal dynamic range and brightness of
% green GECIs, such as GCaMP6, were massively improved (Chen
% et al., 2013), their highly cooperative Ca 2+ binding, with Hill coef-
% ficients ranging from 2 to 4..."
% It is not necessarily the same as the number of binding sites in
% indicator, should be fitted to data ideally (here fitted only for
% 1EqBindingKinetics model.
coop_binding = true;
if ~coop_binding
    n = 1;
else
    if double_exp_conv_model
        n = 4;
    else
        n = n_prime;
    end
end

% define time axis and variables for time integration
t_fl = t;                                           % Calcium sampling rate same as ephys (kept t_fl output for consistency with previous implementation, though now is unnecessary)
dt = t(2)-t(1);
sp = sp*delta_ca_sp;
ca = zeros(size(sp));
ca_b = zeros(size(sp));
j_out = zeros(size(sp));

% assign initial conditions
if ca_0 == 0
    ca_0 = ca_rest;     % default value
end
ca(1) = ca_0;
if ~double_exp_conv_model
    ca_b(1) = ind_con*ca_0^n_prime/(K_d^n_prime + ca_0^n_prime);    % eqn 10.7 Helmchen's handbook... he considers k_on and k_off of cooperative kinetics to be different than k_on and k_off of non-cooperative. He calls (k_off/k_on)_cooperative = K_a and sets it equal to K_d^n_prime
end

for k=1:length(t)
    if k == 1
        ref_k = 1;
        j_out(k) = 0;
    else
        ref_k = k-1;
        if non_lin_extrusion
            j_out(k) = v_max*area/vol*((ca(ref_k))/(ca(ref_k)+K_M)-(ca_rest)./(ca_rest+K_M));   % j_out - j_leak
        else
            j_out(k) = ext_rate*(ca(ref_k) - ca_rest);
        end
    end
    k_b = ind_con*n*ca(ref_k)^(n-1)*K_d^n/(ca(ref_k)^n + K_d^n)^2;
    j_in = sp(k);

    ca(k) = ca(ref_k) + (-dt*j_out(k) + j_in) / ...
        (1 + k_s + k_b);
    
    if ~double_exp_conv_model
        ca_b(k) = ca_b(ref_k) + ...
            dt*(k_on*ca(ref_k)^n_prime*(ind_con - ca_b(ref_k)) ...
            - k_off*ca_b(ref_k));
        if ca_b(k) > ind_con
            ca_b(k) = ind_con;
        end
    end
end

if double_exp_conv_model
    % ca_b
    h = h_kernel(A, t_on, t_off, dt);
    if ~iscolumn(h)
        h = h';
    end
    ca_b = convn(ca-ca_rest, h, 'full') + ca_rest;
    ca_b = ca_b(1:length(ca));

    % delta(F)/F
    fl = fluo_sat_eq(ca_b, ca_rest, K_d, max_dF_F);
else
    fl = ind_con - ca_b + phi*ca_b;
    ca_b_rest = ind_con*ca_rest^n_prime/(K_d^n_prime + ca_rest^n_prime);
    B_rest = ind_con - ca_b_rest;
    fl_0 = B_rest + phi*ca_b_rest;
    fl = (fl - fl_0)/fl_0;
end

% add noise
if SNR ~= 0
    % if there are no spikes, fl is a vector of zeros, SNR is NaN use
    % ranrn with a fized std of 0.15 to add noise in this case else use
    % awgn
    if unique(fl) == 0
        fl = fl + 0.15*randn(size(fl));
    else
        fl = awgn(fl,SNR,'measured');
    end
end

if ~iscolumn(fl)
    fl = fl';
end

if any(isinf(fl))
    fl = nan(size(fl));
end

if ip.Results.PlotTraces
    cols = brewermap(8, 'Dark2');
    figure()
    hold on
    subplot(4,1,1)
    plot(t_fl,ca,'DisplayName','Ca','LineWidth',2,'Color',cols(7,:))
    legend()
    box off
    subplot(4,1,2)
    plot(t_fl,ca_b,'DisplayName','Ca_b','LineWidth',2,'Color',cols(4,:))
    legend()
    box off
    subplot(4,1,3)
    plot(t_fl,j_out,'DisplayName','j_{out}','LineWidth',2,'Color',cols(5,:))
    legend()
    box off
    subplot(4,1,4)
    hold on
    plot(t_fl,fl,'DisplayName','\Delta F/F','LineWidth',2,'Color',cols(2,:))
    plot(t(sp~=0),zeros(1,length(t(sp~=0))),...
        'LineStyle', 'none',...
        'marker','o',...
        'MarkerSize',5,...
        'MarkerEdgeColor',cols(8,:),...
        'MarkerFaceColor',cols(8,:))
    set(gca,'TickDir','out');
    legend()
    box off
    xlabel('Time (s)');
    set(gcf,'color',[1,1,1])
end
end

function h = h_kernel(A, tau_on, tau_off, dt)
ker = @(z) A*(1 - exp(-z/tau_on)).*exp(-z/tau_off);
t_max = log((1/tau_on + 1/tau_off)*tau_off)*tau_on;
ker_max = ker(t_max);
t_end = -log(ker_max*1e-3/A)*tau_off;
h     = ker(0:dt:t_end+dt);
end

