function out = mx_f(f, win, t_obs_i, dt, ca_imaging_frate, varargin)
%f = movmean(f, win);
f = f(1:round(1/(dt*ca_imaging_frate)):t_obs_i);
out = max(f)-f(1);
end