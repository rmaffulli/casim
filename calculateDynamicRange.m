function max_dff = calculateDynamicRange(indicator, varargin)
% the function calculates the max deltaF/F in the 1 eq. kinetics model
% given the indicator and, if required, model varaibles
if nargin == 2
    model_params = varargin{1};
    k_off = model_params(1);
    n_prime = model_params(2);
    phi = model_params(3);
else
    % load optimized values for k_off and n_prime as they do not impact the
    % dynamic range anyways
    if strcmp(indicator,'GCaMP6s')
        load('CaSim_1EqBindingKinetics_GCaMP6s_constants.mat')
    elseif strcmp(indicator,'GCaMP6f')
        load('CaSim_1EqBindingKinetics_GCaMP6f_constants.mat')
    end
    k_off =     casim_constants.k_off;
    n_prime =   casim_constants.n_prime;
    phi = casim_constants.phi;
end

t = linspace(0,10,10000)';
%n_spikes = 1:100;
%max_fl = zeros(size(n_spikes));
%sp = zeros(size(t));
sp = ones(size(t));
%for n = 1:length(n_spikes)
%    n_sp = n_spikes(n);
%    sp(1:n_sp) = 1;
    [~,fl, ca, ca_b] = generate_trace_CaSim(t,sp,indicator,0,0,...
        'ModelConstants',[k_off n_prime phi]);
%    max_fl(n) = max(fl);
%end

%max_dff = max(max_fl);
max_dff = max(fl);
if false
    figure()
    plot(n_spikes,max_fl)
end
end

