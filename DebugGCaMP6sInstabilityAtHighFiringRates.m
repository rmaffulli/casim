clear all
close all
clc

rng('default')
cols = brewermap(8, 'Dark2');

n_rates = 3;       % total amount of rates
% color palette for raster plots (having only 3 rates
rates_sigma_raster_cols(1,:,:) = brewermap(8, 'Blues');
rates_sigma_raster_cols(2,:,:) = brewermap(8, 'Greens');
rates_sigma_raster_cols(3,:,:) = brewermap(8, 'Oranges');

ln_style_ca0 = [":","--","-"];
scatter_color = cols(8,:);

%% definition of main data
indicator = "GCaMP6s";
[~, K_d] = get_ca_sat_params(indicator); 

ca_rest = 50.0E-9;                              % 50 nMol resting Ca concentration used in CaSim

baseline_rate = 0.5;
mean_rate = 30 + baseline_rate;
sigma_dt_ratio = 10;

signoiseratio = 0;
ca_smoothing_win = 1;
t_obs = 1;                                      % observation time through which we count spikes and measure metrics
t_buffer = 3;                                   % buffer time with no spikes to allow calcium decay and have a reasonable estimation of F_0
t_max = t_obs + t_buffer;
dt = 1E-3;                                      % e-phys timestep
ca_imaging_frate = 100;                         % frame rate to be used by calcium imaging data. They are effectively generated at the e-phys timestep dt and then downsampled to simulate various acquisition rates
dt_plots = 1e-4;                                % timestep for plots
n_trials = 50;                                  % number of realizations at each average rate, sigma, indicator and initial calcium concentration
time_obs = linspace(0,t_obs,t_obs/dt)';
time_max = linspace(0,t_max,t_max/dt)';
t_obs_i = t_obs/dt;                             % final timestep e-phys
ind_conc = 10e-6;

    
[rate, t_tot] = gen_rate(time_obs,sigma_dt_ratio,mean_rate,baseline_rate,t_buffer);
% avoid cases with 0 spikes
sp_times = [];
while isempty(sp_times)
    sp = poisson_spike_gen(t_tot,rate,0);
    sp_times = t_tot(sp(1:t_obs_i) == 1);
end
% plot raster
figure(1)
hold on
plot(sp_times, zeros(size(sp_times)),...
    'LineStyle', 'none',...
    'marker','o',...
    'MarkerSize',3,...
    'MarkerEdgeColor',rates_sigma_raster_cols(1,6,:),...
    'MarkerFaceColor',rates_sigma_raster_cols(1,6,:));
plt.Annotation.LegendInformation.IconDisplayStyle = 'off';


ca_0 = ca_rest;
% generate calcium
[t_fl, fl] = generate_trace_CaSim(t_tot,sp,indicator,...
    ca_0,signoiseratio,'IndConc',ind_conc,'PlotTraces', true);
figure(1)
% plot calcium
plot(t_fl(1:round(1/(dt*ca_imaging_frate)):t_obs_i),...
    fl(1:round(1/(dt*ca_imaging_frate)):t_obs_i),...
    'LineStyle',ln_style_ca0{3},'LineWidth',2,...
    'Color',rates_sigma_raster_cols(1,6,:),...
    'DisplayName',"IndConc = " + sprintf('%.2e',ind_conc))
%ylim([-1 10])

legend('box', 'off', 'Location', 'Best');
title({"$\bar{\lambda} =$ "+sprintf('%.2f',ln_style_ca0{3}) + ...
    " [Hz]", "$\sigma / \Delta t =$"+sprintf('%.2f',sigma_dt_ratio)},...
    'interpreter','latex');
xlabel('t [s]')

ylabel('\Delta F/F_0 [-]')
set(gca,'TickDir','out')

ax = findobj(gcf, 'type', 'axes');
set(ax,'LineWidth',2)
set(ax,'FontSize',10);




%% auxiliary  functions
function [rate, time_tot] = gen_rate(t,sigma,mean_rate,baseline_rate,buff_time)
% buff_time is the buffer time (scalar) with zero reate to be appended to t
% needed to correctly find f_0
if buff_time == 0
    time_tot = t;
else
    dt = t(2) - t(1);
    t_tot = t(end) + buff_time;
    time_tot = linspace(0,t_tot,t_tot/dt)';
end
rate = normpdf(t,0.25*max(t),sigma);
rate = rate/mean(rate)*(mean_rate-baseline_rate);
rate = rate + baseline_rate;
% append buffer time with zero rate
if buff_time ~= 0
    rate = [rate; zeros(length(time_tot)-length(t),1)];
end
end