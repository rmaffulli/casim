close all
clear all
clc

indicators = {"GCaMP6f", "GCaMP6s"};

t = linspace(0,10,10000)';
n_spikes = 1:100;
for i = 1:length(indicators)
    max_fl = [];
    max_ca = [];
    max_ca_b = [];
    sp = zeros(size(t));
    ind = indicators{i};
    for n = 1:length(n_spikes)
        n_sp = n_spikes(n);
        sp(1:n_sp) = 1;
        [~,fl, ca, ca_b] = generate_trace_CaSim(t,sp,ind,0,0);
        max_fl = [max_fl max(fl)];
        max_ca = [max_ca max(ca)];
        max_ca_b = [max_ca_b max(ca_b)];
    end
    
    figure(i)
    
    
    p = panel();
    p.pack(3,1)
    p.de.margin = 10;
    
    p(1,1).select();
    plot(n_spikes,max_ca)
    
    p(2,1).select();
    plot(n_spikes,max_ca_b)
    
    p(3,1).select();
    plot(n_spikes,max_fl)
end