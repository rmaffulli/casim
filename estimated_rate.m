function out = estimated_rate(f, win, t_obs_i, dt, ca_imaging_frate,...
    t, ind, varargin)
f = f(1:round(1/(dt*ca_imaging_frate)):t_obs_i);
t = t(1:round(1/(dt*ca_imaging_frate)):t_obs_i);
%f = movmean(f, win);
% f_spline_fit = fit(t,f,'smoothingspline','smoothingParam',0.999);
% f = feval(f_spline_fit,t);
f_prime = diff(f)/(t(2)-t(1));
f = f(1:end-1);
switch ind
    case 'GCaMP6f'
        %Roughly estimated from GCaMP6sf 1AP V1 [s] from Chen et al. Nat. 2013
        %Though the authors of Deneux et al. do not say what it is exactly,
        %it seems to be the approximate time that it takes for the indicator
        %to go back to initial fluorescence (different from tau_1/2
        %reported in Chen and from decay constant ofthe exponential, which
        %is the time it takes to go to 63.2% of max value)
        tau = 0.5;
    case 'GCaMP6s'
        %Roughly estimated from GCaMP6s 1AP V1 [s] from Chen et al. Nat. 2013
        %Same value used in Deneux et al. (they only have GCaMP6s). It
        %seems to be the approximate time that it takes for the indicator
        %to go back to initial fluorescence (different from tau_1/2
        %reported in Chen and from decay constant ofthe exponential, which
        %is the time it takes to go to 63.2% of max value)
        tau = 2;
    otherwise
        error('Unknown GCaMP type!')
end
out = mean(f_prime + f/tau);
end
